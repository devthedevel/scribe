package xyz.devthedevel.scribe.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import xyz.devthedevel.scribe.R
import xyz.devthedevel.scribe.db.Book

public class BooksFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_books, container, false)
    }

    //TODO Use constructor?
    fun populateBooksUi(books: List<Book>) {
        Log.d("", "")
    }

}