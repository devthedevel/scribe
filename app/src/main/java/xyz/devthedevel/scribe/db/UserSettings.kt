package xyz.devthedevel.scribe.db

data class UserSettings(
    var uid: String? = null,
    var theme: String? = "light"
)