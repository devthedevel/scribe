package xyz.devthedevel.scribe.db

import java.util.*


data class Book(
    val author: String,
    val dateCreated: Date
)