package xyz.devthedevel.scribe

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import xyz.devthedevel.scribe.db.Book
import xyz.devthedevel.scribe.db.UserSettings
import xyz.devthedevel.scribe.fragments.BooksFragment
import java.util.*

class MainActivity : AppCompatActivity() {

    companion object {
        private val RC_AUTH: Int = 0
    }

    /*
    Firebase
     */
    private lateinit var mAuth: FirebaseAuth
    private lateinit var mDb: FirebaseFirestore

    /*
    UI
     */
    private lateinit var mDrawerLayout: DrawerLayout
    private lateinit var mDrawerToggle: ActionBarDrawerToggle
    private lateinit var mToolbar: Toolbar

    private lateinit var mPrefs: SharedPreferences

    private var mUserSettings: UserSettings? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mPrefs = this.getPreferences(Context.MODE_PRIVATE)
        mAuth = FirebaseAuth.getInstance()
        mDb = FirebaseFirestore.getInstance()

        initView()
        initUser()
    }

    private fun initView() {
        /*
        Toolbar
         */
        mDrawerLayout = findViewById(R.id.drawer_layout)
        mToolbar = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
        mDrawerToggle = ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close)
        mDrawerLayout.addDrawerListener(mDrawerToggle)
    }

    private fun initUser() {
        val isFirstRun = mPrefs.getBoolean(getString(R.string.prefkey_is_first_run), true)

        if (isFirstRun) {
            //Prompt user to login
            val authProviders = arrayListOf(
                AuthUI.IdpConfig.EmailBuilder().build(),
                AuthUI.IdpConfig.PhoneBuilder().build(),
                AuthUI.IdpConfig.GoogleBuilder().build(),
                AuthUI.IdpConfig.AnonymousBuilder().build()
            )
            startActivityForResult(AuthUI.getInstance()
                .createSignInIntentBuilder()
                .enableAnonymousUsersAutoUpgrade()
                .setAvailableProviders(authProviders).build(), RC_AUTH)
        } else {
            postLogin()
        }
    }

    private fun postLogin() {
        getUserSettings()
        getBooks()
    }

    private fun getUserSettings() {
        val user = mAuth.currentUser
        if (user != null) {
            mDb.collection("users").document(user.uid).get().addOnSuccessListener {doc ->
                if (doc.data != null) {
                    Log.d("Scribe", "Document retrieved")
                    mUserSettings = doc.toObject<UserSettings>()
                } else {
                    Log.d("Scribe", "No document with user UID: ${user.uid}")
                    mUserSettings = UserSettings(uid = user.uid)
                    mUserSettings?.let { userSettings ->
                        mDb.collection("users").document(user.uid).set(userSettings)
                            .addOnSuccessListener {
                                Log.d("Scribe", "User settings uploaded for user UID: ${user.uid}")
                            }
                            .addOnFailureListener {
                                    e -> Log.w("Scribe", "Error writing user settings for UID: ${user.uid}", e)
                            }
                    }
                }
                updateUi()
            }.addOnFailureListener {e -> Log.w("Scribe", "Error retrieving user settings for UID: ${user.uid}", e) }
        }
    }

    private fun getBooks() {
        val user = mAuth.currentUser
        if (user != null) {
            mDb.collection("books").whereEqualTo("author", "/users/${user.uid}").get().addOnSuccessListener {documents ->
                Log.d("Scribe", "Retrieved ${documents.size()} documents for user ${user.uid}")

                if (documents.isEmpty) {
                    val data = Book("/user/${user.uid}", Date())
                    mDb.collection("books").add(data)
                        .addOnSuccessListener {
                            Log.d("Scribe", "Created book collection for user ${user.uid}")

                            val booksFragment = BooksFragment()
                            booksFragment.populateBooksUi(documents.toObjects<Book>())
                            supportFragmentManager.beginTransaction().add(R.id.main_content, booksFragment).commit()
                        }
                        .addOnFailureListener {
                            Log.e("Scribe", "Unable to create book collection for user ${user.uid}")
                        }
                } else {
                    val booksFragment = BooksFragment()
                    booksFragment.populateBooksUi(documents.toObjects<Book>())
                    supportFragmentManager.beginTransaction().add(R.id.main_content, booksFragment).commit()
                }
            }
        }
    }

    private fun updateUi() {
        val user = mAuth.currentUser
        if (user != null) {
            val textNavHeadUser = findViewById<TextView>(R.id.nav_head_user)
            val textNavHeadAccount = findViewById<TextView>(R.id.nav_head_account)

            //Populate navigation header with user details
            if (!user.isAnonymous) {
                textNavHeadUser.visibility = View.VISIBLE
                textNavHeadUser.text = user.displayName
                textNavHeadAccount.text = user.email
            }

            textNavHeadAccount.setOnClickListener {
                val authProviders = arrayListOf(
                    AuthUI.IdpConfig.EmailBuilder().build(),
                    AuthUI.IdpConfig.PhoneBuilder().build(),
                    AuthUI.IdpConfig.GoogleBuilder().build()
                )
                startActivityForResult(AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .enableAnonymousUsersAutoUpgrade()
                    .setAvailableProviders(authProviders).build(), RC_AUTH)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_AUTH) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                val isFirstRun = mPrefs.getBoolean(getString(R.string.prefkey_is_first_run), true)
                if (isFirstRun) mPrefs.edit().putBoolean(getString(R.string.prefkey_is_first_run), false).apply()

                postLogin()
                Toast.makeText(this, "Logged in!", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, response?.error?.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

}
